

void read_csv()
{

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* historical stock price table from yahoo.finance since March 2013
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
 
    valu[ 0] = loadTable("./csv/DJI.csv", "header");

    valu[ 2] = loadTable("./csv/JNJ.csv", "header");
    valu[ 3] = loadTable("./csv/KO.csv", "header");
    valu[ 4] = loadTable("./csv/NKE.csv", "header");
    valu[ 5] = loadTable("./csv/PG.csv", "header");
    
    valu[ 7] = loadTable("./csv/DIS.csv", "header");
    valu[ 8] = loadTable("./csv/HD.csv", "header");
    valu[ 9] = loadTable("./csv/MCD.csv", "header");
    valu[10] = loadTable("./csv/WMT.csv", "header");


    valu[13] = loadTable("./csv/GS.csv", "header");

    valu[15] = loadTable("./csv/TRV.csv", "header");
    valu[16] = loadTable("./csv/V.csv", "header");

    valu[18] = loadTable("./csv/MRK.csv", "header");
    valu[19] = loadTable("./csv/PFE.csv", "header");
    valu[20] = loadTable("./csv/UNH.csv", "header");

    valu[22] = loadTable("./csv/BA.csv", "header");
    valu[23] = loadTable("./csv/CAT.csv", "header");



    valu[27] = loadTable("./csv/UTX.csv", "header");

    valu[29] = loadTable("./csv/CVX.csv", "header");
    valu[30] = loadTable("./csv/XOM.csv", "header");

    valu[32] = loadTable("./csv/AAPL.csv", "header");
    valu[33] = loadTable("./csv/CSCO.csv", "header");




    valu[38] = loadTable("./csv/VZ.csv", "header");
    

}