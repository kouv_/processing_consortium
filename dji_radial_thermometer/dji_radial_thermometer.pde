/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...  MSDS 6390 - Visualization of Information
/* ...
/* ...  Homework 8 - 09-mar-2018
/* ...
/* ...    patrick mcdevitt
/* ...
/* ...  Dow Jones Industrial stocks, 2013 - 2017
/*
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
 
 /* ...    This file includes 1 visualization :
 /* ...        - radially oriented 'thermometer' showing daily closing prices
 /* ...            of the DJI stocks, grouped by market sector
 
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    This project provides a view of the history of the individual stock
/* ...    prices in the Dow Jones Industrial Index - which is comprised of
/* ...    30 stocks in 8 different market sectors.
/* ...    Depicted are the daily adjusted closing prices for each of the stocks from
/* ...    Mar-2013 thru Mar-2018 (5 year history)
/* ...
/* ...    - This visualization depicts :
/* ...        each stock price history is dynamically updated on its radial thermometer,
/* ...        comprised of 50 radially increasing annulus sector.
/* ...        the DJI index is also included with its own thermometer
/* ...        the current closing price is shown as complementary color to the base color
/* ...        the sector colors correspond to a market sector
/* ...        (legend for colors is displayed)
/* ...
/* ...    User Input :
/* ...        - mouse pressed freezes activity
/* ...    
/* ...       
/* ...    - Data behind the scene :
/* ...
/* ...        - database of dow jones industrials closing prices (.csv files)
/* ...            (Yahoo Finance)
/* ...
/* ...        - library of images for the CEOs
/* ...            (basic web search + image transforms)
/* ...
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */



/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    Class Structure Definitions
/* ...
/* ...    Annulus_sector() - basic element to create a sector of an annulus
/* ...    Thermometer() - assemble a collection of Annulus_sectors in a radial stack
/* ...    Legend() - creates legend to identfy market sectors
/* ...
/* ...
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* define global variables
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int n_stocks = 40;
Table[] valu = new Table[n_stocks];

float[] max_value = new float[n_stocks];
float[] min_value = new float[n_stocks];

int[] sector_data = {1, 0, 1, 1, 1, 1, 0, 1, 1, 1,
                     1, 0, 0, 1, 0, 1, 1, 0, 1, 1,
                     1, 0, 1, 1, 0, 0, 0, 1, 0, 1,
                     1, 0, 1, 1, 0, 0, 0, 0, 1, 0};
                        
String[] market_sect = {"Index", "", "Consumer Goods", "Consumer Goods", "Consumer Goods", "Consumer Goods", "Consumer Services",
                "Consumer Services", "Consumer Services", "Consumer Services", "", "Financial", "Financial", "Financial", "Financial",
                "Financial", "", "Healthcare", "Healthcare", "Healthcare", "", "Industrial", "Industrial", "Industrial", "Industrial",
                "Industrial", "Industrial", "", "Oil&Gas", "Oil&Gas", "", "Technology", "Technology", "Technology", "Technology",
                "Technology", "", "Telecommunications", ""};
                
int[] mrkt_sctr_num = {1, 0, 2, 2, 2, 2, 0, 3, 3, 3,
                       3, 0, 4, 4, 4, 4, 4, 0, 5, 5,
                       5, 0, 6, 6, 6, 6, 6, 6, 0, 7,
                       7, 0, 8, 8, 8, 8, 8, 0, 9, 0};
                        
int n_mrkt_sctrs = 10;
color[] mrkt_sctr_clr = new color[n_mrkt_sctrs];
color[] mrkt_mrkr_clr = new color[n_mrkt_sctrs];
color[] scheme = {#f46d43, #888888, #1b9e77, #d95f02, #7570b3, #e0298c,
                #66a61e, #e6ab02, #4575b4, #b44575};
                    
color[] complement = {#000000, #e12f2f , #9e1b84, #02d9cb, #b37570, #298ce0,
                        #5e1ea6, #02afe6, #b44575, #4575b4};

PImage bkg_image;
String img_name = "alertsbg.jpg";
String[] trade_date = new String[1300];


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... begin OOP variables             ...*/
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

Annulus_sector[][] sector_;
Thermometer[] thermo_;

int n_sectors = n_stocks;
int n_rings = 50;
float ring_min = 100;
float ring_del = 6;
float sector_del = 2*PI/float(n_sectors);

int n_sector_ = n_sectors * n_rings;
int n_lines_min_max = 999999;


Legend legend_;
float[] sector_value = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
String[] sector_text = {
    "",
    "Index",
    "Consumer Goods",
    "Consumer Services",
    "Financial",
    "Healthcare",
    "Industrial",
    "Oil&Gas",
    "Technology",
    "Telecommunications"
};

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* setup() routine
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

void setup()
{
    size(1000, 900);
    
    bkg_image = loadImage(img_name);
    
//     noLoop();
    
    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    initialize annulus sectors                                ... */
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
    
    read_csv();
    
    sector_ = new Annulus_sector[n_sectors][n_rings];
    thermo_ = new Thermometer[n_stocks];
        
    for (int im = 0; im < n_mrkt_sctrs; im++)
    {
        mrkt_sctr_clr[im] = scheme[im];
        mrkt_mrkr_clr[im] = complement[im];
    }
    
    for (int is = 0; is < n_sectors; is++)
    {
        float tmp_angle_start = is * sector_del;
        float tmp_angle_end = tmp_angle_start + sector_del;
        
        for (int ir = 0; ir < n_rings; ir++)
        {
            float tmp_rad_inner = ring_min + float(ir) * ring_del;
            float tmp_rad_outer = tmp_rad_inner + ring_del;
            color r_color = mrkt_sctr_clr[mrkt_sctr_num[is]];
            
            int indx = is * n_rings + ir;            
            sector_[is][ir] = new Annulus_sector(tmp_rad_inner, tmp_rad_outer,
                                                tmp_angle_start, tmp_angle_end, r_color);           
        }
        
        color t_color = mrkt_sctr_clr[mrkt_sctr_num[is]];
        
        thermo_[is] = new Thermometer(sector_[is], n_rings, t_color);
    }
    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    find range for each stock (max, min) for scaling 
/* ...    and number of lines in each data file (use min n_lines)    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    for (int ieq = 0; ieq < n_stocks; ieq++)
    { 
        if(sector_data[ieq] == 1)
        {
            int n_lines_ieq = valu[ieq].getRowCount();
            
            if (n_lines_ieq < n_lines_min_max) {n_lines_min_max = n_lines_ieq;}
            
            min_value[ieq] = valu[ieq].getFloat(0, 5);
            
            for (int il = 0; il < n_lines_ieq; il++)
            {
                float cur_value = valu[ieq].getFloat(il, 5);
                
                if (cur_value > max_value[ieq]) {max_value[ieq] = cur_value;}
                if (cur_value < min_value[ieq]) {min_value[ieq] = cur_value;}
            }
        }
    }
  
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    store dates for display
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    for (int it = 0; it < valu[0].getRowCount(); it++)
    {
        trade_date[it] = valu[0].getString(it, 0);
    }
    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... legends and text annotation            
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    legend_ = new Legend(sector_value, sector_text);
    
    strokeWeight(0);
}


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* main draw routine
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int il = 5;

void draw()
{
color value_color;
float cur_value;
    
    image(bkg_image, 0, 0);
    legend_.update(sector_value);
    market_date(trade_date[il]);
    
    translate(width/2, height/2);
    
        for (int ieq = 0; ieq < n_stocks; ieq++)
        {
            if (sector_data[ieq] == 1)
            {
                thermo_[ieq].set_default_color();
                
                for (int ias = 2; ias > 0; ias--)
                {
                    cur_value = valu[ieq].getFloat(il-ias, 5);            
                    value_color = mrkt_mrkr_clr[mrkt_sctr_num[ieq]];
                    
                    /* choose ring to color based on range within stock max-min experience    */
                    
                    int clr_row = int((cur_value - min_value[ieq])
                                    / (max_value[ieq] - min_value[ieq])
                                    * n_rings + 0.5);
                                    
                    clr_row = clamp(clr_row, 0, n_rings-1);
            
    /* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
    /* ...    use oop functions of Thermometer to change display           */
    /* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
    
                    thermo_[ieq].set_color(value_color, clr_row);
                    thermo_[ieq].display();
                }
            }
        }
    
    /* ...    increments line of data to use (each successive day)            */
    /* ...    resets back to 1st day in data set when last date reached        */

    if(!mousePressed)
    {
        il++;
        if (il > n_lines_min_max-1) {il = 5;}
    }    
    
} // ... end of draw() 


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    clamp function returns value within restrictive range     */
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int clamp(int v, int min, int max)
{
    if (v < min) {v = min;}
    if (v > max) {v = max;}

    return v;
}