


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* displays market date
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


void market_date(String date_string)
{
PFont f_logo_1;
f_logo_1 = createFont("Ubuntu", 25, true);

smooth();
    
    textFont(f_logo_1);
    textAlign(CENTER);
    textLeading(0);
    fill(120);
    text(date_string, 7*width/8, 3*height/40);

}