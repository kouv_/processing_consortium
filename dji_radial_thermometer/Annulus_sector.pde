

class Annulus_sector
{

// properties

    float rad_inner;
    float rad_outer;
    float angle_start;
    float angle_end;
    color color_inner = 126;
    color color_border = 10;
    float width_border = 1;
    String text = "";

// constructors

    Annulus_sector(){}
    
    Annulus_sector(float rad_inner, float rad_outer, float angle_start, float angle_end, 
        color color_inner, color color_border, float width_border, String text)
    {
            this.rad_inner = rad_inner;
            this.rad_outer = rad_outer;
            this.angle_start = angle_start;
            this.angle_end = angle_end;
            this.color_inner = color_inner;
            this.color_border = color_border;
            this.width_border = width_border;
            this.text = text;
    }
 
    Annulus_sector(float rad_inner, float rad_outer, float angle_start, float angle_end, 
        color color_inner)
    {
            this.rad_inner = rad_inner;
            this.rad_outer = rad_outer;
            this.angle_start = angle_start;
            this.angle_end = angle_end;
            this.color_inner = color_inner;
    }
    
    
    Annulus_sector(float rad_inner, float rad_outer, float angle_start, float angle_end)
    {
            this.rad_inner = rad_inner;
            this.rad_outer = rad_outer;
            this.angle_start = angle_start;
            this.angle_end = angle_end;
            this.color_inner = color_inner;
            this.color_border = color_border;
            this.width_border = width_border;
            this.text = text;
    }


// general methods
 
    int display(color sector_color)
    {
    PShape annulus_sector;
    float x1, y1, x2, y2;
          // Create the shape
  
          annulus_sector = createShape();
          
          annulus_sector.beginShape();
        
          // Make shapes          
              x1 = rad_inner * cos(angle_start);
              y1 = rad_inner * sin(angle_start);
              x2 = rad_outer * cos(angle_start);
              y2 = rad_outer * sin(angle_start);
            
              annulus_sector.vertex(x2, y2);
              annulus_sector.vertex(x1, y1);
            
              for (float theta = angle_start; theta <= angle_end; theta += 0.1)
              {
                 annulus_sector.vertex(rad_inner * cos(theta), rad_inner * sin(theta));
              }
              annulus_sector.vertex(rad_inner * cos(angle_end), rad_inner * sin(angle_end));
            
              x1 = rad_inner * cos(angle_end);
              y1 = rad_inner * sin(angle_end);
              x2 = rad_outer * cos(angle_end);
              y2 = rad_outer * sin(angle_end);
              
              annulus_sector.vertex(x1, y1);
              annulus_sector.vertex(x2, y2);
              
              for (float theta = angle_end; theta >= angle_start; theta -= 0.1)
              {
                 annulus_sector.vertex(rad_outer * cos(theta), rad_outer * sin(theta));
              }
              annulus_sector.vertex(rad_outer * cos(angle_start), rad_outer * sin(angle_start));
          annulus_sector.endShape(CLOSE);
          
          annulus_sector.setFill(sector_color);
          annulus_sector.setStroke(60);

        shape(annulus_sector);
        return 1;
    }
    
// setters and getters (mutators and accessors)

 
}