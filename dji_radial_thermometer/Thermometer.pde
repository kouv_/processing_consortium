


class Thermometer
{

// properties

    Annulus_sector[] sector_;
    color[] sector_color = new color[n_rings];
    int n_rows;
    color default_color = color(40);

// constructors

    Thermometer(){}

    Thermometer(Annulus_sector[] sector_, int n_rows)
    {
            this.sector_ = sector_;
            this.n_rows = n_rows;
    }
    
    Thermometer(Annulus_sector[] sector_, int n_rows, color default_color)
    {
            this.sector_ = sector_;
            this.default_color = default_color;
            this.n_rows = n_rows;
    }

    Thermometer(Annulus_sector[] sector_, color[] sector_color, int n_rows, color default_color)
    {
            this.sector_ = sector_;
            this.sector_color = sector_color;
            this.n_rows = n_rows;
            this.default_color = default_color;
    }

// general methods
 
    int display()
    {
        for (int ir = 0; ir < n_rows; ir++)
        {            
            sector_[ir].display(sector_color[ir]);
        }
        return 1;
    }
    
// setters and getters (mutators and accessors)

     void set_default_color()
     {
         for(int ir = 0; ir < n_rows; ir++)
         {
             this.sector_color[ir] = default_color;
         }
     }

 
     void set_color(color row_color, int irow)
     {
         this.sector_color[irow] = row_color;
     }
}