//SUNNA 
//Super of bPic
class Blackhole {
  //Location
  float blackx;
  float blacky;
  //Movement
  int farOut;
  int shrink;
  //Color
  color strokeColor;
  float strokeweight;
  
  Blackhole() {}
  
  Blackhole(float blackx, float blacky) {
    this.blackx=blackx;
    this.blacky=blacky;
    strokeColor=color(random(100, 255), random(100, 255), random(100, 255), 20);
  }
 
  void update() {
    //Increase the distance out
    farOut = 200+shrink;
    shrink -=1;

  }
 
  void display() {
    //Set the Stroke Color
    stroke(strokeColor);
    strokeWeight(strokeweight);
    fill(255,1);
    ellipse(blackx, blacky, farOut, farOut);
  }
 //Collision
  boolean dead() {
    if(farOut < 1) {
      return true;
    }
    return false;
  }
}

class bPic  extends Blackhole{
  

  bPic(float blackx, float blacky){
    super(blackx, blacky);
  }
  
  void swirl(){
  pushMatrix();
  translate(blackx, blacky);
  rotate(PI+woo);
  translate(-blackx, -blacky);

  image(bhole, blackx, blacky);
  woo-=.01;
  popMatrix();
  }

}