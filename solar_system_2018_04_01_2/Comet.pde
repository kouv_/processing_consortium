
class Comet {

  ArrayList<Tail> tails;    // An arraylist for all the tails
  PVector loc;                   // An origin point for where tails are birthed
  PImage img;

  Comet(int num, PVector v, PImage img_) {
    tails = new ArrayList<Tail>();              // Initialize the arraylist
    loc = v.copy();                                   // Store the origin point
    img = img_;
    for (int i = 0; i < num; i++) {
      tails.add(new Tail(loc, img));         // Add "num" amount of tails to the arraylist
    }
  }

  void run() {
    for (int i = tails.size()-1; i >= 0; i--) {
      Tail p = tails.get(i);
      p.run();
      if (p.isDead()) {
        tails.remove(i);
      }
    }
  }

  // Method to add a force vector to all tails currently in the system
  void applyForce(PVector dir) {
    // Enhanced loop!!!
    for (Tail p : tails) {
      p.applyForce(dir);
    }
  }  

  void addTail() {
    tails.add(new Tail(loc, img));
  }
}



class Tail {
  PVector loc;
  PVector vel;
  PVector acc;
  float lifespan;
  PImage img;

  Tail(PVector l, PImage img_) {
    acc = new PVector(0, 0);
    float vx = randomGaussian()*0.3;
    float vy = randomGaussian()*0.3 - 1.0;
    vel = new PVector(vx, vy);
    loc = l.copy();
    lifespan = 100.0;
    img = img_;
  }

  void run() {
    update();
    render();
  }

  // Method to apply a force vector to the Tail object
  // Note we are ignoring "mass" here
  void applyForce(PVector f) {
    acc.add(f);
  }  

  // Method to update position
  void update() {
    vel.add(acc);
    loc.add(vel);
    lifespan -= 2.5;
    acc.mult(0); // clear Acceleration
  }

  // Method to display
  void render() {
    imageMode(CENTER);
    tint(255, lifespan);
    image(img, loc.x, loc.y);
    // Drawing a circle instead
    // fill(255,lifespan);
    // noStroke();
    // ellipse(loc.x,loc.y,img.width,img.height);
  }

  // Is the tail still useful?
  boolean isDead() {
    if (lifespan <= 0.0) {
      return true;
    } else {
      return false;
    }
  }
}