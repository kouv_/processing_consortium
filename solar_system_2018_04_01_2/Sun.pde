

class Sun extends Celestial_body
{
    
// super class properties
//    PVector loc;
//    color body_color;
//    float radius;
   
// Sun specific properties
    
    int id;
    String name;
    float mass;

    Sun()
    {
        super();
    }
    
    Sun(PVector loc, float radius, color body_color, int id, String name, float mass)
    {
        super(loc, radius, body_color);
        this.id = id;
        this.name = name;
        this.mass = mass;
    }
    
    void display()
    {
        fill(body_color);
        pushMatrix();
            translate(loc.x, loc.y, loc.z);
            sphere(radius);
        popMatrix();
    }
    
    boolean isHit()
    {
        if (dist(mouseX, mouseY, loc.x, loc.y) < radius)
        {
            return true;
        } 
        return false;
    }
      
}