
// super class

class Celestial_body
{
    
// properties
    PVector loc;
    color body_color;
    float radius;
    
//constructors
    
    Celestial_body() {}
    
    Celestial_body(PVector loc, float radius, color body_color)
    {
        this.loc = loc;
        this.body_color = body_color;
        this.radius = radius;
    }
    
// methods

    void construct() {}
    void display() {}
    void move() {}

}