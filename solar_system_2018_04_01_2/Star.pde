
// The stars start out dark

class Star extends Celestial_body
{
    
// super class properties
//    PVector loc;
//    color body_color;
//    float radius;
   
// Star specific properties

    float bright = -1;
    float change;
    
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...        constructors        
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
    
    Star()
    {
        super();
    }
    
    Star(PVector loc, float radius, color body_color, float bright, float change)
    {
        super(loc, radius, body_color);
        this.bright = bright;
        this.change = change;
    }
  
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...        methods        
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

// When a star fades out, make it reappear in a random place within the sketch.
// make the new star of random size and brightness

    void shine()
    {     
// set the brightness and draw the star

        fill(bright);
        ellipse(loc.x, loc.y, radius, radius);
         
// change the brightness

        bright += change;
         
// if a star achieves maximum brightness
// choose a random fade out speed

        if(bright > 255)
        {
            bright = 255;
            change = random(-1, -3);
        }
        else if(bright < 0)
        {
            loc.x = random(width);
            loc.y = random(height);
            radius = random(3);            // random sizes btn 1 $ 3
            change = random(1, 5);         //change in brightness
            bright = 0;
        }

    }
}
 