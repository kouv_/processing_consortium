class Planet extends Celestial_body
{
    
// super class properties
//    PVector loc;
//    color body_color;
//    float radius;
   
// Planet specific properties
// orbital motion (planar) : r = [a*(1-e²)]/[1 + e * cos(theta)]
    
    int id;
    String name;
    color orig_color;
    int sun_id;            /* id of sun to which the planet belongs */
    float orbit_mean_radius;
    float orbit_eccentricity;
    float orbit_period;
    float orbit_phase_offset;
    float orbit_a;
    
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...        constructors        
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
    
    Planet()
    {
        super();
    }
    
    Planet(PVector loc, float radius, color body_color, int id, String name)
    {
        super(loc, radius, body_color);
        this.id = id;
        this.name = name;
        orig_color = body_color;
    }
    
    Planet(PVector loc, float radius, color body_color, int id, String name,
                int sun_id,
                float orbit_mean_radius,
                float orbit_eccentricity,
                float orbit_period,
                float orbit_phase_offset,
                float orbit_a)
    {
        super(loc, radius, body_color);
        this.id = id;
        this.name = name;
        orig_color = body_color;
        this.sun_id = sun_id;
        this.orbit_mean_radius = orbit_mean_radius;
        this.orbit_eccentricity = orbit_eccentricity;
        this.orbit_period = orbit_period;
        this.orbit_phase_offset = orbit_phase_offset;
        this.orbit_a = orbit_a;
}

/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...        methods        
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

    void display()
    {
        pushMatrix();
            translate(loc.x, loc.y, loc.z);
            sphere(radius);
        popMatrix();
    }
    
    void revolve(PVector sun_loc, float tau)
    {   
        float theta, e2;
        float e = orbit_eccentricity;
        float r = orbit_mean_radius;
        
        theta = 2 * PI * tau / orbit_period;
        e2 = orbit_eccentricity * orbit_eccentricity;
        
        r = (orbit_a * (1 - e2)) / (1 + e * cos(theta));
        
        loc.x = r * cos(theta);
        loc.y = r * sin(theta);
        loc.z = loc.y;
                
        fill(body_color);
        pushMatrix();
            translate(loc.x + sun_loc.x, loc.y + sun_loc.y, loc.z + sun_loc.z);
            sphere(radius);
        popMatrix();
    }
    
    boolean isHit()
    {
        if (dist(mouseX, mouseY, loc.x, loc.y) < radius)
        {
            return true;
        } 
        return false;
    }
      
    void resetCol()
    {
        body_color = orig_color;
    }
}