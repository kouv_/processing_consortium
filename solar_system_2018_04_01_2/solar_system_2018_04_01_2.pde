
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...    global variables                                                 */
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

JSONArray planet_data;
Planet[] orb;
Sun sun;
//SUNNA 
ArrayList<Asteroid> asteroid=new ArrayList<Asteroid>();
ArrayList<Blackhole> singularity = new ArrayList<Blackhole>();
PImage bhole;
float woo;
bPic bpic;
float m;
Comet comet;

int star_count = 500;
Star[] star = new Star[star_count];

/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...    setup                                                      */
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

void setup()
{
    size(900, 725, P3D);
    smooth();
    noStroke();
//    ortho();
    
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...      planet properties
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

    planet_data = loadJSONArray("planets.json");

    int num_planets = planet_data.size();
    orb = new Planet[num_planets];
    
    for (int i = 0; i < num_planets; i++)
    {
        JSONObject planet = planet_data.getJSONObject(i);
        float rad = planet.getFloat("mass");
        String planetType = planet.getString("composition");
        String hex_color = planet.getString("color");
        color bd_clr = unhex(hex_color.replace("#", "FF"));
        int sun_id = planet.getInt("sun_id");
        float mean_rad = planet.getFloat("mean_radius");
        float eccentricity = planet.getFloat("eccentricity");
        float period = planet.getFloat("period");
        float phase = planet.getFloat("phase");
        float apsis = planet.getFloat("apsis");
                        
        orb[i] = new Planet(new PVector(width/2, height/2-25),
                rad, bd_clr, i, planetType,
                sun_id, mean_rad, eccentricity, period, phase, apsis);

    }
    
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...      sun properties
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

        sun= new Sun (new PVector(width/4, 3*height/4, 0),
                    50, color(200, 200, 0), 1, "The Sun", 100);
  
         
                    
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...      star properties
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

    for(int i = 0; i < star_count; i++)
    {
        star[i] = new Star(new PVector(random(width), random(height)),
                    random(3.), color(0), 0, random(1,5));
    }
  //SUNNA  
  imageMode(CENTER);
  bhole= loadImage("blackhole.png");
  bhole.resize(50,50);
  bpic= new bPic(600, 50);
  PImage img = loadImage("particle.png");
  img.resize(30, 30);
  comet = new Comet(0, new PVector(width, height-60), img);
}

/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/
/* ...    draw routine                                                      */
/* ...    -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-    ...*/

int second;
float tau;
PVector sun_loc;

void draw()
{
    
    
    background(25);
    //SUNNA 
tint(255, 165);
   bpic.swirl();
  //Create infinite
  if( millis() % 2000 < 1000 ){ 
    Blackhole o = new Blackhole(600, 50);
    singularity.add(o);
  }
 //Run blackhole
  for(int i = 0; i < singularity.size(); i ++) {
    //Run the Wave methods
    singularity.get(i).update();
    singularity.get(i).display();
   
 //Collision
    if(singularity.get(i).dead()) {
      //If so, delete it
      singularity.remove(i);
    }
   }
   
    sun_loc = new PVector();
    shininess(45);
    lightSpecular(255, 255, 255);
    directionalLight(254, 254, 254, 1, 1, -1);
    specular(225, 225, 225);
    ambientLight(150, 150, 150);
    ambient(90, 90, 90);
    
    spotLight(255., 255., 100.,
                sun.loc.x, sun.loc.y, sun.loc.z+4*sun.radius,
                0, 0, -1, PI/4, 0.5);
 
    second++;
    tau = second/2;
    
/* ...    stars shine            */
    
    for(int i = 0; i < star_count; i++)
    {
        star[i].shine();
    }
    

/* ...    revolve planets about sun            */

    for (int i = 0; i < orb.length; i++)
    {
        orb[i].revolve(sun.loc, tau);
    }
    
/* ...    sun display and migration            */
   
    sun.display();
    
    sun.loc.x += random(-0.5, 1.2);
    sun.loc.y += random(-1.2, 0);
    sun.loc.z += random(-1, -5);
  //SUNNA
         if( millis() % 10000 < 100 ){                
    Asteroid x = new Asteroid (new PVector(-100, height/2, 0),
                    10, color(100), 1, "The Asteroid", 100);
    asteroid.add(x);
   }
   for(int i = 0; i < asteroid.size(); i ++) {
    asteroid.get(i).display();
    
    asteroid.get(i).loc.x += random(0, 1.2);
    asteroid.get(i).loc.y += random(-1.2, 0);
    asteroid.get(i).loc.z += random(-1, -5);
   
    }

  

pushMatrix();
translate(0-m/5, 0-m*2,0-m*1.5);
  float dx = map(-50, 100, width, -0.2, -2);
  PVector wind = new PVector(dx, 0);
  comet.applyForce(wind);
  comet.run();
  for (int i = 0; i < 2; i++) {
    comet.addTail();
  }
m+=1;
popMatrix();


}