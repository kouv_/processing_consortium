PFont Font1;

int[][] data;
EatingDrinking et;
Travel tr;
Sports sp;
VolunteerService vs;
CivicService cs;

import controlP5.*;


void setup() {
  size(1200, 800);
  background(0);
  
  data = new int[0][5];
  et = new EatingDrinking();
  tr = new Travel();
  sp = new Sports();
  vs = new VolunteerService();
  cs = new CivicService();
  
  //loadData(0, 100, 0, 100, 0, 100, 0, 24, 0, 24);
  loadData();
  slider = slider();
}


/* HASHMAP IDEA */
//HashMap<Integer,[]Float> hm = new HashMap<Integer,[]Float>();

//hm.put(1 , {125, 200} );
//hm.put(2 , {125, 200} );

//xloc = hashmap.get(1)[0] 
//yloc = hashmap.get(1)[1]

float x = random(0,width);
float y = random(0,height);

void draw() {
  Font1 = createFont("Calibri Bold", 24);
  fill(255);
  textFont(Font1);
  text("A Day in the Life of Americans (based on 2016 American Time Use Survey)", 250, 50);
  clear();
  // ADD STATIC LOCATIONS INSTEAD OF RANDOM
  // 1. CHANGE SCALE OF BUBBLES SO ALL ARE LARGE ENOUGH SINCE TIME QUERY MAKES AVERAGE SIZE TOO SMALL
  for (int num = 1; num < 18;  num++) {
    et.drawBubble(x, y, slider.getValue(), num);
      //et.drawBubble(125, (height-100)/2, slider.getValue(),1);
      //tr.drawBubble(width/2, (height-100)/2, slider.getValue(),2);
      //sp.drawBubble(width-125, (height-100)/2, slider.getValue(),3);
      //vs.drawBubble(width/2+(width/2-125)*2/4, (height-100)/2+112.5, slider.getValue(),18);
      //cs.drawBubble(125+(width/2-125)*2/4, (height-100)/2+112.5, slider.getValue(),5);
    }
  //print(slider.getValue());
}