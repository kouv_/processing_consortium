
class DynamicObject extends Graph {
  // percent of screen to traverse per frame
  float spd;
  float pos_x;
  float pos_y;
  
  DynamicObject(float origin_x,float origin_y,float x_scale,
                  float y_scale, float _spd) {
    super(origin_x,origin_y,x_scale,y_scale);
    spd = _spd;
  }
  
  // move towards a target
  void move(float[] target) {
      float tar_x = target[0];
      float tar_y = target[1];
  }
}