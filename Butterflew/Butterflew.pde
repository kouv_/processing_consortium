CyanFlower[] cyan = new CyanFlower[4];
BlueFlower[] blue = new BlueFlower[4];
RedFlower[] red = new RedFlower[4];
PurpleFlower[] purple = new PurpleFlower[4];
DynamicObject wing;

float xc, yc;

// universe 
Metronome metro;

void setup() {
  size(1000,750);
  //init theta with zero movement
  wing = new DynamicObject(0,0,width,height,0.0);
  metro = new Metronome(wing.spd);
      
  xc = width/2;
  yc = height/2;
  
  for(int i=0; i<4; i++)
  {
  cyan[i] = new CyanFlower(xc, yc);
  blue[i] = new BlueFlower(xc, yc);
  red[i] = new RedFlower(xc, yc);
  purple[i] = new PurpleFlower(xc, yc);
  }
}

void draw() {
  //clear old frames
  clear();
  background(255, 217, 179); 
  
  fill(9, 48, 109);
  metro._increment(50, 250);
    quad(width/2, height*0.5, //top right
        width-metro.posX*.5, height-metro.posY,  //top left
        width-metro.posX*.5, height-metro.posY * .5,  // bottom left
        width/2, height*0.5   //bottom right
        );
    quad(width/2, height*0.5, //top right
        metro.posX*.5, metro.posY,  //top left
        metro.posX*.5, metro.posY * .5,  // bottom left
        width/2, height*0.5   //bottom right
        );
        
    quad(width/2, height*0.5, //top right
        metro.posX*.5, height-metro.posY,  //top left
        metro.posX*.5, height-metro.posY * .5,  // bottom left
        width/2, height*0.5   //bottom right
        );
    quad(width/2, height*0.5, //top right
        width-metro.posX*.5, metro.posY * .5,  //top left
        width-metro.posX*.5, metro.posY,  // bottom left
        width/2, height*0.5   //bottom right
        );
        
 
   for(int i=0; i<4; i++)
   {
   cyan[i].display(); 
   blue[i].display(); 
   red[i].display(); 
   purple[i].display(); 
   }
  
   purple[0].move0();
   purple[1].move1();
   purple[2].move2();
   purple[3].move3();
  
   blue[0].move4();
   blue[1].move5();
   blue[2].move6();
   blue[3].move7();
   
   red[0].move8();
   red[1].move11();
   red[2].move12();
   red[3].move14();
   
   cyan[0].move9();
   cyan[1].move10();
   cyan[2].move13();
   cyan[3].move15();     
    
}