class RedFlower extends Flower{
 
// constructors
  RedFlower()
  {
    super();    
  }

  RedFlower(float x, float y)
  {   
    super(x,y);
  }

  void display()
  {
    fill(255,0,0);
    
    ellipse(x,    y-20, 10, 40);
    ellipse(x,    y+20, 10, 40);
    ellipse(x+20, y,    40, 10);
    ellipse(x-20, y,    40, 10);
    ellipse(x,    y,    10, 10);
    
    pushMatrix();
        fill(255,153,204);
        
        translate(x, y);
        rotate(PI/4);  
        
        ellipse(  0, -20, 10, 40);
        ellipse(  0,  20, 10, 40);
        ellipse( 20,   0, 40, 10);
        ellipse(-20,   0, 40, 10);
        ellipse(  0,   0, 10, 10);
    popMatrix();
  }
  
}