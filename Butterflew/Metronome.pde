/* Standalone class for movement universe */
class Metronome {
    float posX, posY, radiusX, radiusY, theta;

Metronome(float _theta) {
  theta = _theta;
  posX = posY = 0;
  radiusX = 50;
  radiusY = 100;
  }
  
// sets the pace for other movement
float[] _increment(float x, float y) {
    this.theta += 0.05;
    this.posX = x * cos( theta );
    this.posY = y * sin( theta );
    float[] store = {posX, posY};
    return store;
  }
}