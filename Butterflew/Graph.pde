class Graph {
  float origin_x;
  float origin_y;
  float x_end;
  float y_end;
  
Graph(float origin_x,float origin_y,float x_scale,float y_scale) {
  this.origin_x = origin_x;
  this.origin_y = origin_y;
  this.x_end = x_scale;
  this.y_end = y_scale;
  }
float map_x(float input, float start, float end) {
  return(map(input, start, end, origin_x, x_end));
  }
  
float map_y(float input, float start, float end) {
  return(map(input, start, end, origin_y, y_end));
  }
}