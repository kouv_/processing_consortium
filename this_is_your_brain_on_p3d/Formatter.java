import processing.core.*;
import processing.data.*;

// Data File Struct
// Country Name | Country Code | Indicator Name | Indicator Code | 1995 | 2000 | 2005 | 2010 | 2014

// Redundant class design
public class Formatter {
  Table table;
  
  Formatter(PApplet p, String tar, Boolean header_bool) {
    if (header_bool) {
      this.table = p.loadTable(tar, "header");
    } else {
      this.table = p.loadTable(tar);
    }
  }
  
}