import processing.core.PApplet;

public class PlotLine {      
  PApplet parent; // The parent PApplet that we will render ourselves onto
  float x;
  float y;
  float z;

  PlotLine(PApplet p, float x, float y, float z) {
    parent = p;
    x = x;
    y = y;
    z = z;
  }
  
  // Draw stripe
  void display() {
    parent.box(this.x,this.y,this.z);
  }
}