import processing.core.*;
import processing.data.*;

public class this_is_your_brain_on_p3d extends PApplet {
   Formatter data;
  
  public void settings() {
     size(640, 360, P3D);
  }

  public void setup() {
    // Country Name | Country Code | Indicator Name | Indicator Code | 1995 | 2000 | 2005 | 2010 | 2014
    this.data = new Formatter(this, "wealthdata.csv", true);
    // background
    background(0);  
  }

  public void draw() {
    // data vars
    String country;
    String indicator;
    float f_1995;
    float f_2000;
    float f_2005;
    float f_2010;
    float f_2014;  
    
    for (TableRow row : this.data.table.rows()) {
      country = row.getString("Country Name");
      indicator = row.getString("Indicator Name");
      f_1995 = row.getFloat("1995");
      f_2000 = row.getFloat("2000");
      f_2005 = row.getFloat("2005");
      f_2010 = row.getFloat("2010");
      f_2014 = row.getFloat("2014"); 
      //PlotLine x;
      //x = new PlotLine(this, f_1995, f_2000, f_2005);
      //x.display()  
  }
 
    // Test Code //
    //pushMatrix();
    //translate(130, height/2, 0);
    //rotateY(1.25);
    //rotateX(-0.4);
    //noStroke();
    //box(100);
    //popMatrix();

    //pushMatrix();
    //translate(500, height*0.35, -200);
    //noFill();
    //stroke(255);
    //sphere(280);
    //popMatrix();
    
  }
}