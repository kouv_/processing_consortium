Flower[] f = new Flower[16];
float xc, yc;


void setup()
{
    size(800, 800); ///*, P3D*/);
    background(80);
    
    xc = width/2;
    yc = height/2;
    
    for(int i=0; i<16; i++){
    f[i] = new Flower(xc, yc);
 }
    
}

void draw()
{
 background(80);
 for(int i=0; i<16; i++){
 f[i].display(); 
 }
 
 f[0].move0();
 f[1].move1();
 f[2].move2();
 f[3].move3();
 f[4].move4();
 f[5].move5();
 f[6].move6();
 f[7].move7();
 f[8].move8();
 f[9].move9();
 f[10].move10();
 f[11].move11();
 f[12].move12();
 f[13].move13();
 f[14].move14();
 f[15].move15(); 

}