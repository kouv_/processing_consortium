Flower[] f = new Flower[16];
float xc, yc;

int n_papillon = 50;
float[][] x = new float[n_papillon][3];
float[][] y = new float[n_papillon][3];

float[][] xt = new float[n_papillon][3];
float[][] yt = new float[n_papillon][3];
float[] theta = new float[n_papillon];
color[] wing = new color[n_papillon];
void setup()
{
    size(800, 800, P3D);
    background(80);
    xc = width/2;
    yc = height/2;
    
    for(int i=0; i<16; i++){
    f[i] = new Flower(xc, yc);
    }
    smooth();
    
    for (int ip = 0; ip < n_papillon; ip++)
    {
        x[ip][0] = random(20, width-20);
        y[ip][0] = random(20, height-20);
        x[ip][1] = x[ip][0] + random(-10, 100);
        y[ip][1] = y[ip][0] + random(-10, 100);
        x[ip][2] = x[ip][0] + random(-20, 80);
        y[ip][2] = y[ip][0] + random(-20, 80);
        
        theta[ip] = atan2(y[ip][1]-y[ip][0], x[ip][1]-x[ip][0]);
        
        wing[ip] = color(150+random(-50,50), 50+random(0, 200), 80+random(-20,170));
    }
    
//    noLoop();
}

float phi;
float second;



void draw()
{
    background(80);
    for(int i=0; i<16; i++){
     f[i].display(); 
    }
    fill(120);
    
    phi += 0.02;
    
    for (int ip = 0; ip < n_papillon; ip++)
    {
        theta[ip] = atan2(y[ip][1]-y[ip][0], x[ip][1]-x[ip][0]);
        
        fill(200, 0, 0);
        ellipse(x[ip][0], y[ip][0], 10, 10);
        fill(10, 100, 50);
        ellipse(x[ip][1], y[ip][1], 10, 10);
        fill(20, 50, 90, 30);
        triangle(x[ip][0], y[ip][0], x[ip][1], y[ip][1], x[ip][2], y[ip][2]);
    
        for (int i = 0; i < 3; i++)
        {
            xt[ip][i] =      (x[ip][i]-x[ip][0]) * cos(theta[ip]) + (y[ip][i]-y[ip][0]) * sin(theta[ip]);
            yt[ip][i] = -1 * (x[ip][i]-x[ip][0]) * sin(theta[ip]) + (y[ip][i]-y[ip][0]) * cos(theta[ip]);
        }
    
        fill(200, 200, 100, 40);
        pushMatrix();
            translate(x[ip][0], y[ip][0]);
            rotateZ(theta[ip]);
            
            rotateX(phi);
            
            fill(120, 130, 30);
            ellipse(0+2, 0+2, 10, 10);
            fill(10, 10, 50);
            ellipse(xt[ip][1]-xt[ip][0]-2, yt[ip][1]-yt[ip][0]-2, 10, 10);
        
            fill(wing[ip]);
            triangle(0, 0, xt[ip][1], yt[ip][1], xt[ip][2], yt[ip][2]);
        
        popMatrix();
        
        for (int jj = 0; jj < 3; jj++)
        {
            x[ip][jj] += random(-1, 0.9);
            y[ip][jj] += random(-1, 0.9);
        }
        
     
    }
    
 f[0].move0();
 f[1].move1();
 f[2].move2();
 f[3].move3();
 f[4].move4();
 f[5].move5();
 f[6].move6();
 f[7].move7();
 f[8].move8();
 f[9].move9();
 f[10].move10();
 f[11].move11();
 f[12].move12();
 f[13].move13();
 f[14].move14();
 f[15].move15(); 

}