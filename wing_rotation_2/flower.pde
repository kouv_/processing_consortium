class Flower
{
//properties
    float x;
    float y;
    
//contstructors
    Flower(){}

    Flower(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    
// general methods

  void display()
  {
    fill(0, 206, 209);
    
    ellipse(x,    y-20, 10, 40);
    ellipse(x,    y+20, 10, 40);
    ellipse(x+20, y,    40, 10);
    ellipse(x-20, y,    40, 10);
    ellipse(x,    y,    10, 10);
    
    pushMatrix();
        fill(200, 100, 50);
        
        translate(x, y);
        rotate(PI/4);  
        
        ellipse(  0, -20, 10, 40);
        ellipse(  0,  20, 10, 40);
        ellipse( 20,   0, 40, 10);
        ellipse(-20,   0, 40, 10);
        ellipse(  0,   0, 10, 10);
    popMatrix();
  }
  
  // The moving flowers
 
  // SE flower
  void move0(){
    if (x <= width ){
      x+=.85;
      y+=.85;
    }
    else {
      x = width/2;
      y = height/2;
    }      
   }
  
  // NW flower
  void move1(){      
    if (x >= width-width ){
      x-=.85;
      y-=.85;
    }
    else {
      x = width/2;
      y = height/2;
     }  
    }
  
  // NE flower
  void move2(){
    if (x <= width ){
      x+=.85;
      y-=.85;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   }
   
  // SW flower 
  void move3(){      
    if (x >= width-width){
      x-=.85;
      y+=.85;
    }
    else {
      x = width/2;
      y = height/2;
     }
    }
    
  // W flower
  void move4(){
    if (x >= (width-width) ){
      x-=1;
    }
    else {
      x = width/2;
      y = height/2;
     }
    }
    
   // N flower  
   void move5(){
    if (y >= height-height){
      y-=1;
    }
    else {
      x = width/2;
      y = height/2;
     }
    }
    
   // E flower
   void move6(){      
    if (x <= width ){
      x++;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   } 
    
   // S flower  
   void move7(){
    if (y <= height){
      y++;
    }
    else {
      x = width/2;
      y = height/2;
     }
    }
    
  // NNE flower  
  void move8(){
    if (x <= width ){
      x+=.5;
      y-=1;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   }
   
  // ENE flower  
  void move9(){      
    if (x <= width ){
      x+=1;
      y-=.5;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   }    
    
  // ESE flower
  void move10(){
    if (x <= width ){
      x+=1;
      y+=.5;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   }  
    
  // SSE flower  
  void move11(){
    if (x <= width ){
      x+=.5;
      y+=1;
    }
    else {
      x = width/2;
      y = height/2;
    } 
   }
    
   // NNW flower 
   void move12(){
    if (x >= width-width ){
      x-=.5;
      y-=1;
    }
    else {
      x = width/2;
      y = height/2;
     }
   }
    
   // ENE flower 
   void move13(){
    if (x >= width-width ){
      x-=1;
      y-=.5;
    }
    else {
      x = width/2;
      y = height/2;
     }
   }
    
   // SSW flower
   void move14(){
    if (x >= width-width){
      x-=.5;
      y+=1;
    }
    else {
      x = width/2;
      y = height/2;
     }
   }
   
   //WSW flower
   void move15(){
    if (x >= width-width){
      x-=1;
      y+=.5;
    }
    else {
      x = width/2;
      y = height/2;
     }
   }
   
  
}