

class Flower
{
//properties
    float x;
    float y;
    
//contstructors
    Flower(){}

    Flower(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    
// general methods

  void display()
  {
    fill(0, 206, 209);
    
    ellipse(x,    y-20, 10, 40);
    ellipse(x,    y+20, 10, 40);
    ellipse(x+20, y,    40, 10);
    ellipse(x-20, y,    40, 10);
    ellipse(x,    y,    10, 10);
    
    pushMatrix();
        fill(200, 100, 50);
        
        translate(x, y);
        rotate(PI/4);  
        
        ellipse(0, -20, 10, 40);
        ellipse(0,  20, 10, 40);
        ellipse( 20, 0, 40, 10);
        ellipse(-20, 0, 40, 10);
        ellipse(0, 0, 10, 10);
    popMatrix();
  }
  
  void move(){
    x+=1;
    y+=1;
    
  }
  
  
  
  
  
  
  
  
  
}