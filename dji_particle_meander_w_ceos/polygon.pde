

void polygon(float x, float y, float rad, int sides, float stroke_wgt, color stroke_color)
{
    float theta = 0;
    float rotate_ang = TWO_PI / sides;
    
    strokeWeight(stroke_wgt);
    stroke(stroke_color);
    noFill();
    
    beginShape();
        float x2 = 0; float y2 = 0;
        
        for (int i = 0; i < sides; i++)
        {
            x2 = x + 2 * rad * cos(theta);
            y2 = y + 2 * rad * sin(theta);
            vertex(x2, y2);
            theta += rotate_ang;
        }
        
    endShape(CLOSE);
}