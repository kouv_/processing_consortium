



PFont f_title, f_body, f_logo_1, f_logo_2;

class Scoreboard
{
// properties

    float[] metric_value;
    String[] metric;

// constructors

    Scoreboard(){}
    
    Scoreboard(float[] metric_value, String[] metric)
    {
        this.metric_value = metric_value;
        this.metric = metric;
    }

// general methods
    
    void update(float[] metric_value)
    {  
    f_title = createFont("Ubuntu", 15, true);
    f_body = createFont("Ubuntu", 15, true);
    f_logo_1 = createFont("Ubuntu", 15, true);
    f_logo_2 = createFont("Ubuntu", 15, true);
    
    smooth();
    char bullet = '•';

        textFont(f_title);
        textAlign(RIGHT);
        textLeading(0);
        fill(90, 90, 90);
    //    text(description[0], width/4, height/6);
    
        textFont(f_body);
        textAlign(LEFT);
        textLeading(20);    
        
         for (int iqx = 1; iqx < metric.length; iqx++)
         {
             fill(120);
             text(metric[iqx], width/20, height/20 + iqx * 20);
         }
         
         textFont(f_title);
         textAlign(RIGHT);
         for (int iqx = 1; iqx < metric.length; iqx++)
         {             
             fill(60);
             text(str(int(metric_value[iqx])), width/30, height/20 + iqx * 20); 
             polygon(width/30-5, height/20 + iqx * 20-5, 5, iqx+2, 3, color(scheme[int(metric_value[iqx])]));
         }

    }
    
// setters and getters (mutators and accessors)

    float[] get_metric_value() {return metric_value;}

}