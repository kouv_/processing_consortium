



class Picture_frame
{

// properties

    float wdth;
    float hght;
    float border_weight;
    color border_color;
    PImage up_image;
    PImage down_image;
    boolean state;
    

// constructors

    Picture_frame(){}
    
    Picture_frame(float wdth, float hght, float border_weight, color border_color,
                    PImage up_image, PImage down_image, boolean state)
    {
            this.wdth = wdth;
            this.hght = hght;
            this.border_weight = border_weight;
            this.border_color = border_color;
            this.up_image = up_image;
            this.down_image = down_image;
            this.state = state;
    }
    

// general methods
 
    int display(boolean state)
    { 
        stroke(border_color);
        

        if(state == true)
        {
            image(up_image, 0, 0);
            this.border_weight = 4;
        } else {
            image(down_image, 0, 0);
            this.border_weight = 1;
        }
        
        noFill();
        strokeWeight(this.border_weight);
        rect(0, 0, this.wdth, this.hght);

        return 1;
    }
    
    
    void move(int dir)
    {
    }
    
// setters and getters (mutators and accessors)

    float get_width()
    {
        return this.wdth;
    }
    
    float get_height()
    {
        return this.hght;
    }

}