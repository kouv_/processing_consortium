/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...  MSDS 6390 - Visualization of Information
/* ...
/* ...  Homework 8 - 09-mar-2018
/* ...
/* ...    patrick mcdevitt
/* ...
/* ...  Dow Jones Industrial stocks, 2013 - 2017
/*
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
 
 /* ...    This file includes 2 visualizations in same display :
 /* ...        - meandering particles for the DJI closing prices
 /* ...        - picture wall demonstrating the reactions of the CEOs
 /* ...            of the DJI companies to the daily market behavior
 
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    This project provides a view of the history of the individual stock
/* ...    prices in the Dow Jones Industrial Index - which is comprised of
/* ...    30 stocks in 8 different market sectors.
/* ...    Depicted are the daily adjusted closing prices for each of the stocks from
/* ...    Mar-2013 thru Mar-2018 (5 year history)
/* ...
/* ...    - This visualization depicts :
/* ...        each stock is identified by a polygonal symbol, along with the DJI Index also
/* ...        the symbol colors correspond to a market sector
/* ...        (legend for colors is displayed)
/* ...        the size of the polygon is proportional to the price of the stock
/* ...        the symbols have heavier outline borders on "up" market days - for that stock
/* ...        and thinner borders of "down" market days
/* ...        the vertical height of the symbol is representative of the 20-day rolling
/* ...        average closing price, for the date displayed.
/* ...
/* ...        the images of the CEOs on the right of the frame flip images between "up" and
/* ...        "down" market experiences. FOr "up" days, the CEO of that company is displayed
/* ...        in their normal business portrait image; on "down" market days, the CEOD imaged 
/* ...        is transformed to "I'm having a bad day" image which was created using image
/* ...        transformation similar to a fun house mirror distortion
/* ...        (see Digital Darkroom, previous assignment)
/* ...
/* ...    User Input :
/* ...        - right mouse freezes activity
/* ...        - left or cetner mouse + drag : scrolls time frame backward or forward,
/* ...            depending on drag to the left or right
/* ...    
/* ...       
/* ...    - Data behind the scene :
/* ...
/* ...        - database of dow jones industrials closing prices (.csv files)
/* ...            (Yahoo Finance)
/* ...
/* ...        - library of images for the CEOs
/* ...            (basic web search + image transforms)
/* ...
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    Class Structure Definitions
/* ...
/* ...    Picture_frame() - basic element to contain an image + border
/* ...    Picture_wall() - assemble a collection of Picture_frames
/* ...    Scoreboard() - creates legend to identfy market sectors
/* ...
/* ...
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */



/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* define global variables
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int n_stocks = 31;
Table[] valu = new Table[n_stocks];

float[] max_value = new float[n_stocks];
float[] min_value = new float[n_stocks];
float[][] r_avg = new float[n_stocks][1300];
float[][] y_avg = new float[n_stocks][1300];
int[][] state = new int[n_stocks][1300];
int n_r_avg = 20;
String[] trade_date = new String[1300];

int[] sector_data = {1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                     1, 0, 1, 1, 1, 1, 1, 1, 1, 0,
                     0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1};
                        
String[] market_sect = {"Index", "Consumer Goods", "Consumer Goods", "Consumer Goods",
                        "Consumer Goods", "Consumer Services", "Consumer Services", "Consumer Services", "Consumer Services",
                        "Financial", "Financial", "Financial", "Financial",
                        "Financial", "Healthcare", "Healthcare", "Healthcare",
                        "Industrial", "Industrial", "Industrial", "Industrial",
                        "Industrial", "Industrial", "Oil&Gas", "Oil&Gas",
                        "Technology", "Technology", "Technology", "Technology",
                        "Technology", "Telecommunications"};
 
int n_mrkt_sctrs = 10;
int[] mrkt_sctr_num = {1, 2, 2, 2, 2, 3, 3, 3, 3, 4,
                       4, 4, 4, 4, 5, 5, 5, 6, 6, 6,
                       6, 6, 6, 7, 7, 8, 8, 8, 8, 8, 9};
                       
color[] mrkt_sctr_clr = new color[n_mrkt_sctrs];
color[] mrkt_mrkr_clr = new color[n_mrkt_sctrs];

color[] scheme = {#f46d43, #888888, #1b9e77, #d95f02, #7570b3, #e0298c,
                    #66a61e, #e6ab02, #4575b4, #b44575};
                    
color[] complement = {#000000, #e12f2f , #9e1b84, #02d9cb, #b37570, #298ce0,
                        #5e1ea6, #02afe6, #b44575, #4575b4};



Scoreboard legend_;
float[] sector_value = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
String[] sector_text = {
    "",
    "Index",
    "Consumer Goods",
    "Consumer Services",
    "Financial",
    "Healthcare",
    "Industrial",
    "Oil&Gas",
    "Technology",
    "Telecommunications"
};

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... picture wall variables         ...*/
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


int n_images = 30;
Picture_frame[] ceo_;
Picture_wall wall_;
boolean[] wall_state = new boolean[n_images+1];

PImage[] ceo_image_up = new PImage[n_images];
PImage[] ceo_image_down = new PImage[n_images];

String[] img_name_up =
    {"./images/up/jnj_gorsky.jpg",
"./images/up/KO_quincey.jpg.jpeg",
"./images/up/NKE_parker.jpeg",
"./images/up/pg_david_taylor.jpg",
"./images/up/DIS_iger.jpeg",
"./images/up/HD_menear.jpeg",
"./images/up/MCD_easterbrook.jpeg",
"./images/up/WMT_mcmillon.jpeg",
"./images/up/AXP_squeri.jpeg",
"./images/up/GS_Blankfein.jpg",
"./images/up/JPM_dimon.jpeg",
"./images/up/TRV_schnitzer.jpeg",
"./images/up/V_kelly.jpeg",
"./images/up/MRK_frazier.jpeg",
"./images/up/PFE_read.jpeg",
"./images/up/united_health_hemsley.jpg",
"./images/up/boeing_muilenburg.jpg",
"./images/up/CAT_umpleby.jpeg",
"./images/up/CAT_umpleby.jpeg",
"./images/up/GE_flannery.jpeg",
"./images/up/jnj_gorsky.jpg",
"./images/up/KO_quincey.jpg.jpeg",
"./images/up/chevron_ecowatch.jpg",
"./images/up/NKE_parker.jpeg",
"./images/up/AAPL_cook.jpeg",
"./images/up/pg_david_taylor.jpg",
"./images/up/DIS_iger.jpeg",
"./images/up/HD_menear.jpeg",
"./images/up/MCD_easterbrook.jpeg",
"./images/up/verizon_mcadam.jpg"};
    
String[] img_name_down = 
    {"./images/down/jnj_gorsky.jpg_-50_200_fhm.png",
"./images/down/KO_quincey.jpg.jpeg_-50_200_fhm.png",
"./images/down/NKE_parker.jpeg_0_100_fhm.png",
"./images/down/pg_david_taylor.jpg_0_100_fhm.png",
"./images/down/DIS_iger.jpeg_-50_100_fhm.png",
"./images/down/HD_menear.jpeg_-50_100_fhm.png",
"./images/down/MCD_easterbrook.jpeg_-50_100_fhm.png",
"./images/down/WMT_mcmillon.jpeg_0_100_fhm.png",
"./images/down/AXP_squeri.jpeg_-50_100_fhm.png",
"./images/down/GS_Blankfein.jpg_-50_200_fhm.png",
"./images/down/JPM_dimon.jpeg_0_100_fhm.png",
"./images/down/TRV_schnitzer.jpeg_-50_200_fhm.png",
"./images/down/V_kelly.jpeg_0_-300_fhm.png",
"./images/down/MRK_frazier.jpeg_0_100_fhm.png",
"./images/down/PFE_read.jpeg_-50_100_fhm.png",
"./images/down/united_health_hemsley.jpg_-50_200_fhm.png",
"./images/down/boeing_muilenburg.jpg_-50_100_fhm.png",
"./images/down/CAT_umpleby.jpeg_-50_100_fhm.png",
"./images/down/CAT_umpleby.jpeg_-50_100_fhm.png",
"./images/down/GE_flannery.jpeg_0_-300_fhm.png",
"./images/down/jnj_gorsky.jpg_-50_200_fhm.png",
"./images/down/KO_quincey.jpg.jpeg_-50_200_fhm.png",
"./images/down/chevron_ecowatch.jpg_0_-200_fhm.png",
"./images/down/NKE_parker.jpeg_0_100_fhm.png",
"./images/down/AAPL_cook.jpeg_-50_100_fhm.png",
"./images/down/pg_david_taylor.jpg_0_100_fhm.png",
"./images/down/DIS_iger.jpeg_-50_100_fhm.png",
"./images/down/HD_menear.jpeg_-50_100_fhm.png",
"./images/down/MCD_easterbrook.jpeg_-50_100_fhm.png",
"./images/down/verizon_mcadam.jpg_-50_100_fhm.png"};

int img_size = 80;
float w, h, sf;
int n_lines_min_max = 999999;
float wdth = 80;
float hght = 80;


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... begin OOP variables             ...*/
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int n_particle = n_stocks;

float n_particle_now = 0;
float birth_rate = 0.04;

float[] x = new float[n_particle];
float[] y = new float[n_particle];
int[] side_count = new int[n_particle];
float[] rad = new float[n_particle];
int[] status = new int[n_particle];

float[] speed_x = new float[n_particle];
float[] speed_y = new float[n_particle];

float[] gravity = new float[n_particle];
float[] damping = new float[n_particle];
float[] friction = new float[n_particle];
float wind = 0.0;

PImage bkg_image;
String img_name = "alertsbg.jpg";


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... begin setup()
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


void setup()
{
    size(1300, 810);
    
    bkg_image = loadImage(img_name);
       
    read_csv();
    
    for (int ieq = 0; ieq < n_stocks; ieq++)
    {
        max_value[ieq] = 0;
        min_value[ieq] = 999999;
    }
    
    for (int it = 0; it < valu[0].getRowCount(); it++)
    {
        trade_date[it] = valu[0].getString(it, 0);
    }

/* ...    20 day rolling average        ... */

    for (int ieq = 0; ieq < n_stocks; ieq++)
    {
        if(sector_data[ieq] == 1)
        {
            int nlines = valu[ieq].getRowCount();
        
            for (int id = 0; id < n_r_avg; id++)
            {
                r_avg[ieq][n_r_avg-1] += valu[ieq].getFloat(id, 5);
                y_avg[ieq][id] = height + 100;
                state[ieq][id] = 1;
            }
            r_avg[ieq][n_r_avg-1] /= float(n_r_avg);
            
            for (int id = n_r_avg; id < nlines; id++)
            {
                float adj_close = valu[ieq].getFloat(id, 5);
                state[ieq][id] = -1;
                r_avg[ieq][id] = r_avg[ieq][id-1]*n_r_avg + adj_close - valu[ieq].getFloat(id-n_r_avg, 5);
                r_avg[ieq][id] /= float(n_r_avg);
                
                if (adj_close > max_value[ieq]) {max_value[ieq] = adj_close;}
                if (adj_close < min_value[ieq]) {min_value[ieq] = adj_close;}
                if (r_avg[ieq][id] > r_avg[ieq][id-1]) {state[ieq][id] = 1;}
            }
            
        /* ...    scale rolling average values to sketch height    */
        
            for (int id = n_r_avg; id < nlines; id++)
            {
                y_avg[ieq][id] = y_convert(r_avg[ieq][id], min_value[ieq], max_value[ieq]);
                println(id, r_avg[ieq][id], y_avg[ieq][id]);
            }
        }
    }
    
    for (int im = 0; im < n_mrkt_sctrs; im++)
    {
        mrkt_sctr_clr[im] = scheme[im];
        mrkt_mrkr_clr[im] = complement[im];
    }

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... legends and text annotation            
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

    legend_ = new Scoreboard(sector_value, sector_text);

    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... particle initialization     ...*/
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
    
    for (int i = 0; i < n_particle; i++)
    {
        x[i] = width/200;
        y[i] = 3*height/4;
        side_count[i] = mrkt_sctr_num[i]+2;
        speed_x[i] = random(4, 8);
        speed_y[i] = 0;
        rad[i] = sqrt(max_value[i]+1);
        gravity[i] = 0.01;
        damping[i] = 0.9;
        friction[i] = 0.5;
        status[i] = 1;
    }
    
    rad[0] /= 10;
    
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... picture wall initialization     ...*/
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
    
    ceo_ = new Picture_frame[n_images];
    
    for (int im = 0; im < n_images; im++)
    {
        println(im);
        println(img_name_up[im]);
        ceo_image_up[im] = loadImage(img_name_up[im]);
        ceo_image_down[im] = loadImage(img_name_down[im]);
        
        w = ceo_image_up[im].width;
        h = ceo_image_up[im].height;
        if (w >= h) {sf = img_size/w;} else {sf = img_size/h;}          
        ceo_image_up[im].resize(int(w * sf), int(h * sf));
        ceo_image_up[im].loadPixels();
        
        w = ceo_image_down[im].width;
        h = ceo_image_down[im].height;
        if (w >= h) {sf = img_size/w;} else {sf = img_size/h;}          
        ceo_image_down[im].resize(int(w * sf), int(h * sf));         
        ceo_image_down[im].loadPixels();
    
        ceo_[im] = new Picture_frame(img_size, img_size, 2,
                        scheme[mrkt_sctr_num[im+1]],
                        ceo_image_up[im],
                        ceo_image_down[im],
                        true);
    }
    
    wall_ = new Picture_wall(ceo_, wall_state, 10, 3, n_images, (width - 3 * img_size), 0);

}

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... end_of_setup()
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */

int second = 0;
int day;
int il = 1;
float today_value, ystrdy_value, delta_value;


/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ... draw()
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


void draw()
{

    fill(0, 5);
    noStroke();
    
    if (!(mousePressed && (mouseButton == RIGHT)))
    {
        image(bkg_image, 0, 0);
    
        second++;   
        day = second / 4;
        il = day + 1;
        
        legend_.update(sector_value);
        market_date(trade_date[il]);
        
        for (int ip = 0; ip < int(n_particle_now+0.5); ip++)
        {
            if(sector_data[ip] == 1)
            {
                if (state[ip][il] > 0)
                {
                    polygon(x[ip], y[ip], rad[ip], side_count[ip], 4, mrkt_sctr_clr[mrkt_sctr_num[ip]]);
                    if (ip > 0) {wall_state[ip-1] = true;}
                } else {
                    
                    println(il, ip, mrkt_sctr_num[ip]);
                    polygon(x[ip], y[ip], rad[ip], side_count[ip], 1, mrkt_sctr_clr[mrkt_sctr_num[ip]]);
                    if (ip > 0) {wall_state[ip-1] = false;}
                }
                
                x[ip] += speed_x[ip] - wind;

                y[ip] = y_avg[ip][il];

                check_collision(ip);
            }
        }
        
        wall_.display(wall_state);
        
        if(n_particle_now < n_particle)
        {
            n_particle_now += birth_rate;
            println(second, n_particle, n_particle_now);
        }
       
        if (il > 1230)
        {
            second = 0;
            n_particle_now = 0;
            il = 1;
            for (int i = 0; i < n_particle; i++)
            {
                x[i] = width/200;
                y[i] = 3*height/4;
                side_count[i] = mrkt_sctr_num[i]+2;
                speed_x[i] = random(4, 8);
                speed_y[i] = 0;
                rad[i] = sqrt(max_value[i]+1);
                gravity[i] = 0.01;
                damping[i] = 0.9;
                friction[i] = 0.5;
                status[i] = 1;
            }
            rad[0] /= 10;
        }
    }
}


void check_collision(int i)
{
    if (x[i] > width - rad[i] - 240)
    {
        x[i] = width - rad[i] - 240;
        speed_x[i] = -speed_x[i];
        speed_x[i] *= damping[i];
        speed_x[i] *= friction[i];
    }
    if (x[i] < rad[i])
    {
        x[i] = rad[i];
        speed_x[i] = -speed_x[i];
        speed_x[i] *= damping[i];
        speed_x[i] *= friction[i];
    }
    
    if (y[i] > height - rad[i])
    {
        y[i] = height - rad[i];
        speed_y[i] = -speed_y[i];
        speed_y[i] *= damping[i];
        speed_y[i] *= friction[i];
    }
    if (y[i] < rad[i])
    {
        y[i] = rad[i];
        speed_y[i] = -speed_y[i];
        speed_y[i] *= damping[i];
        speed_y[i] *= friction[i];
    }

}

String mouse_direction;

void mouseDragged()
{
    ellipse(mouseX, mouseY, 5, 5);
    if (mouseX > pmouseX)
    {
        second++;
    }
    else if (mouseX < pmouseX)
    {
        second--;
    }
    
    clamp(second, 0, 1230*4);
}



int clamp(int v, int min, int max)
{
    if (v < min) {v = min;}
    if (v > max) {v = max;}

    return v;
}