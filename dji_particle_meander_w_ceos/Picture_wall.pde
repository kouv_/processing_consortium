




class Picture_wall
{

// properties

    Picture_frame[] frame_;
    boolean[] state;
    int n_rows;
    int n_cols;
    int n_frames;
    float x;
    float y;
    

// constructors

    Picture_wall(){}
    
    Picture_wall(Picture_frame[] frame_, boolean[] state, int n_rows, int n_cols,
                    int n_frames, float x, float y)
    {
            this.frame_ = frame_;
            this.state = state;
            this.n_rows = n_rows;
            this.n_cols = n_cols;
            this.n_frames = n_frames;
            this.x = x;
            this.y = y;
    }
   
// general methods
 
    int display(boolean[] state)
    {
        pushMatrix();
        
        translate(this.x, this.y);
        
        int ii = 0;
        float x_offset_accum = 0;
        
        for (int ir = 0; ir < n_rows; ir++)
        {
            for (int ic = 0; ic < n_cols; ic++)
            {
                ii = ir * n_cols + ic;
                
                frame_[ii].display(state[ii]);
                
                translate(frame_[ii].get_width(), 0);
                x_offset_accum += frame_[ii].get_width();
            }
            translate(-x_offset_accum, frame_[ii].get_width());
            x_offset_accum = 0;
        }
        
        popMatrix();
        
        return 1;
    }
    
    
    void move(int dir)
    {
    }
    
// setters and getters (mutators and accessors)

 
}