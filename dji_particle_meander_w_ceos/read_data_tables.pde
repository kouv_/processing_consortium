


void read_csv()
{

/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* historical stock price table from yahoo.finance since March 2013
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
 
    valu[ 0] = loadTable("./csv/DJI.csv", "header");
    valu[ 1] = loadTable("./csv/JNJ.csv", "header");
    valu[ 2] = loadTable("./csv/KO.csv", "header");
    valu[ 3] = loadTable("./csv/NKE.csv", "header");
    valu[ 4] = loadTable("./csv/PG.csv", "header");
    valu[ 5] = loadTable("./csv/DIS.csv", "header");
    valu[ 6] = loadTable("./csv/HD.csv", "header");
    valu[ 7] = loadTable("./csv/MCD.csv", "header");
    valu[ 8] = loadTable("./csv/WMT.csv", "header");
    valu[10] = loadTable("./csv/GS.csv", "header");
    valu[12] = loadTable("./csv/TRV.csv", "header");
    valu[13] = loadTable("./csv/V.csv", "header");
    valu[14] = loadTable("./csv/MRK.csv", "header");
    valu[15] = loadTable("./csv/PFE.csv", "header");
    valu[16] = loadTable("./csv/UNH.csv", "header");
    valu[17] = loadTable("./csv/BA.csv", "header");
    valu[18] = loadTable("./csv/CAT.csv", "header");
    valu[22] = loadTable("./csv/UTX.csv", "header");
    valu[23] = loadTable("./csv/CVX.csv", "header");
    valu[24] = loadTable("./csv/XOM.csv", "header");
    valu[25] = loadTable("./csv/AAPL.csv", "header");
    valu[26] = loadTable("./csv/CSCO.csv", "header");
    valu[30] = loadTable("./csv/VZ.csv", "header");
}