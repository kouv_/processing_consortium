
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */
/* ...    converts physical vertical coordinates to sketch vertical coordinates
/* ...
/* ...    input : y, y_min, y_max - physical coord value & range
/* ...
/* ...    returns : converted value in sketch coords
/* -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- */


float y_convert(float y, float y_min, float y_max)
{
    if (!Float.isNaN(y))
    {    
        return(map(y,  y_min, y_max, 0.95 * height, 0.05 * height));
    }
    return y;
   
}